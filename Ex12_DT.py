from sklearn.datasets import load_iris

data = load_iris()
X = data.data
y = data.target

print("Features :", data.feature_names)
print("X-Shape : ",X.shape)
print("y-Shape : ",y.shape)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.35)

from sklearn.tree import DecisionTreeClassifier
dt = DecisionTreeClassifier()
dt.fit(X_train,y_train)
prediction = dt.predict(X_test)

from sklearn.metrics import accuracy_score
print("Accuracy: ", accuracy_score(y_test, prediction))
