from collections import deque

class Node:
    def __init__(self, name, val, childs=[]):
        self.name = name
        self.val = val
        self.childs = childs

def constructTree():
    level = int(input('Enter the number of levels - '))
    name = 65
    root = Node(chr(name),int(input('Enter the value for root - ')))
    name+=1
    q = deque()
    q.append(root)
    i = 1
    while i < level:
        n = len(q)
        for j in range(n):
            current = q.popleft()
            childs = list(map(lambda x: int(x), input(
            'Enter the child values for node {} - '.format((current.name))).split(' ')))
            if(childs[0] == -999):
                continue
                
            children = []
            for child in childs:
                node = Node(chr(name),child)
                children.append(node)
                q.append(node)
                name+=1
                current.childs = children
        i += 1
    return root

def prune(root,ptrigger):
    flag = 0
    for child in root.childs:
        if(flag == 1):
            print(child.name)
        if(child == ptrigger):
            flag = 1
     
def helper(root, level, order, alpha, beta):
    
    if not root.childs:
        return root.val
    
    useMax = chooseMax(order, level)
    
    if useMax:
        current = -999
    else:
        current  = 999

    if useMax:
        for child in root.childs:
            current = max(current, helper(child, level+1, order, alpha, beta))
            alpha= max(alpha, current)      
            if beta<=alpha and (beta != 999 or alpha != -999):
                prune(root,child)
                break  

    else:
        for child in root.childs:
            current = min(current, helper(child, level+1, order, alpha, beta))
            beta= min(beta, current)      
            if beta<=alpha and (beta != 999 or alpha != -999):
                prune(root,child)
                break
        
    root.val = current
    return current


def chooseMax(order, level):
    if order == 1:
        return level % 2 == 0
    elif order == 0:
        return level % 2 != 0

def getPath(root, path):
    if not root.childs:
        return
    
    for child in root.childs:
        if child.val==root.val:
            path.append(child.name)
            getPath(child, path)

def main():
    root = constructTree()
    order = int(input('Enter order max (1) or min (0) - '))
    res = helper(root, 0, order, -999, 999) 
    path = ['A']
    getPath(root, path)
    print('The result is {}'.format(res))
    print('The selected node is - {}'.format(path))
    
main()