on(a,b).
on(b,c).
on(c,table).

on(a,table).
on(b,c).
on(c,table)

:-dynamic on/2.
:-dynamic move/3.

on(a,b).
on(b,c).
on(c,table).

r_put_on(A,B) :-
on(A,B).

r_put_on(A,B) :-
not(on(A,B)),
A \== table,
A \== B,
clear_off(A), /* N.B. "action" used as precondition */
clear_off(B),
on(A,X),
retract(on(A,X)),
assert(on(A,B)),
assert(move(A,X,B)).

clear_off(table). /* Means there is room on table */
clear_off(A) :- /* Means already clear */
not(on(_X,A)).

clear_off(A) :-
A \== table,
on(X,A),
clear_off(X), /* N.B. recursion */
retract(on(X,A)),
assert(on(X,table)),
assert(move(X,A,table))
